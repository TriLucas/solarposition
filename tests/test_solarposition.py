import re
import pytest

import solarposition

from unittest.mock import patch

from datetime import datetime


ABS_ERROR = .011


# Comparison data was constructed with
# https://gml.noaa.gov/grad/solcalc/
# and crosschecked with
# https://keisan.casio.com/exec/system/1224682277
PLACES = [
    (-31.35363, -63.632812),
    (51.33312, 6.5623343),
    (24.766784, -13.886718),
    (42.032974, 91.40625),
    (50.513426, -36.738281),
]

POSITIONS = [
    [(78.35, 37.68),  (54.79, 26.64),  (284.9, 66.06),  (286.91, 34.55),
     (299.38, 4.21)],
    [(136.28, 18.42), (112.84, 31.88), (212.79, 9.75),  (231.66, 20.59),
     (265.71, 32.67)],
    [(112.31, 19.84), (99.22, 46.92),  (213.85, 34.34), (239.65, 39.37),
     (285.38, 22.32)],
    [(117.18, 12.74), (103.02, 31.3),  (195.36, 23.7),  (230.25, 29.28),
     (268.29, 36.71)],
    [(137.66, 19.79), (129.34, 41.35), (214.34, 9.86),  (246.42, 11.96),
     (278.9, 22.14)],
]

DATETIMES = [
    datetime(2022, 2, 21, 10, 0),
    datetime(2022, 4, 21, 10, 0),
    datetime(2000, 1, 4, 15, 0),
    datetime(2032, 10, 5, 16, 32),
    datetime(1994, 7, 8, 18, 0),
]

parameters = [
    (PLACES[place], *POSITIONS[place][dt], DATETIMES[dt])
    for place in range(0, len(PLACES))
    for dt in range(0, len(DATETIMES))
]


@pytest.fixture(scope="session", params=range(0, len(PLACES)))
def reused_calculator(request):
    return (
        solarposition.SolarSkyPov(*PLACES[request.param]),
        POSITIONS[request.param],
    )


def format_assertion_error(dt_obj, latlong, result, expected):
    msg = f'{latlong[0]}:{latlong[1]} @ {dt_obj}; {result} != {expected}'
    assert abs(result - expected) <= ABS_ERROR, msg


def test_caching():
    latlong = PLACES[0]
    dt_obj = DATETIMES[0]
    expected_azimuth, expected_elevation = POSITIONS[0][0]

    to = solarposition.SolarSkyPov(*latlong)
    to.set_jt_from_datetime(DATETIMES[0])

    format_assertion_error(dt_obj, latlong, to.elevation, expected_elevation)
    format_assertion_error(dt_obj, latlong, to.azimuth, expected_azimuth)

    to.set_jt_from_datetime(DATETIMES[0])

    format_assertion_error(dt_obj, latlong, to.elevation, expected_elevation)
    format_assertion_error(dt_obj, latlong, to.azimuth, expected_azimuth)


@pytest.mark.parametrize('latlong,expected_azimuth,expected_elevation,dt_obj',
                         parameters)
def test_azimuth_elevation(latlong, expected_azimuth, expected_elevation,
                           dt_obj):
    to = solarposition.SolarSkyPov(*latlong)
    to.set_jt_from_datetime(dt_obj)
    format_assertion_error(dt_obj, latlong, to.elevation, expected_elevation)
    format_assertion_error(dt_obj, latlong, to.azimuth, expected_azimuth)


@pytest.mark.parametrize('data_idx', range(0, len(DATETIMES)))
def test_reused_calculator(reused_calculator, data_idx):
    to, expected = reused_calculator
    expected_azimuth, expected_elevation = expected[data_idx]
    dt_obj = DATETIMES[data_idx]
    to.set_jt_from_datetime(dt_obj)

    format_assertion_error(dt_obj, (to._latitude, to._longitude), to.azimuth,
                           expected_azimuth)
    format_assertion_error(dt_obj, (to._latitude, to._longitude), to.elevation,
                           expected_elevation)


def _test_cmd(capsys, latlong, expected_azimuth, expected_elevation, d_str,
              t_str, dt_obj):
    with patch("sys.argv", ["solarposition", "-d", d_str, "-t", t_str, "-n",
                            f"{latlong[0]}", "-w", f"{latlong[1]}"]):
        solarposition.main()

        captured = capsys.readouterr()

        match = re.match(r'^.*?: (-?\d+?(?:\.\d+)+?), (-?\d+?(?:\.\d+)+?)$',
                         captured.out)

        assert match is not None

        azimuth = float(match.groups()[0])
        elevation = float(match.groups()[1])

        format_assertion_error(dt_obj, latlong, elevation, expected_elevation)
        format_assertion_error(dt_obj, latlong, azimuth, expected_azimuth)


@pytest.mark.parametrize('t_fmt', solarposition.JulianTime._FMTS['time'])
@pytest.mark.parametrize('d_fmt', solarposition.JulianTime._FMTS['date'])
@pytest.mark.parametrize('latlong,expected_azimuth,expected_elevation,dt_obj',
                         parameters)
def test_interface(latlong, expected_azimuth, expected_elevation, dt_obj,
                   capsys, d_fmt, t_fmt):
    d_str = dt_obj.strftime(d_fmt)
    t_str = dt_obj.strftime(t_fmt)

    _test_cmd(capsys, latlong, expected_azimuth, expected_elevation, d_str,
              t_str, dt_obj)


t_fmts = [('%Hasd', ValueError)] + [
    (fmt, None) for fmt in solarposition.JulianTime._FMTS['time']
]
d_fmts = [('%Yasd', ValueError)] + [
    (fmt, None) for fmt in solarposition.JulianTime._FMTS['date']
]


@pytest.mark.parametrize('t_fmt,t_err', t_fmts)
@pytest.mark.parametrize('d_fmt,d_err', d_fmts)
def test_formatting(capsys, d_fmt, t_fmt, d_err, t_err):
    dt_obj = DATETIMES[0]
    latlong = PLACES[0]

    expected_azimuth, expected_elevation = POSITIONS[0][0]

    d_str = dt_obj.strftime(d_fmt)
    t_str = dt_obj.strftime(t_fmt)

    ex_err = t_err or d_err

    if ex_err is None:
        _test_cmd(capsys, latlong, expected_azimuth, expected_elevation, d_str,
                  t_str, dt_obj)
    else:
        with pytest.raises(ex_err):
            _test_cmd(capsys, latlong, expected_azimuth, expected_elevation,
                      d_str, t_str, dt_obj)
