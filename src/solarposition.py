import argparse
import math
import pytz

from timezonefinder import TimezoneFinder
from datetime import datetime
from datetime import timedelta


class JulianTime:
    _FMTS = {
        'time': [
            '%H:%M:%S',
            '%H:%M',
        ],
        'date': [
            '%d.%m.%Y',
            '%d-%m-%Y',
            '%Y%m%d',
            '%Y/%d/%m',
            '%Y-%d-%m',
        ],
    }

    @classmethod
    def _p_fmt(cls, date: str, time: str):
        _fmts = [f'{d_fmt} {t_fmt}'
                 for d_fmt in cls._FMTS['date']
                 for t_fmt in cls._FMTS['time']]
        for fmt in _fmts:
            try:
                return datetime.strptime(f'{date} {time}', fmt)
            except ValueError:
                pass
        raise ValueError(f'Invalid format: {date} {time}')

    @classmethod
    def from_str(cls, date: str, time: str, tz: pytz.BaseTzInfo = None):
        dt_obj = cls._p_fmt(date, time)
        return cls(tz.localize(dt_obj))

    @classmethod
    def from_datetime(cls, datetime_obj: datetime, tz: pytz.BaseTzInfo = None):
        if datetime_obj.tzinfo is None:
            if tz is None:
                raise ValueError('Cannot handle a naive datetime!')
            return cls(tz.localize(datetime_obj))
        return cls(datetime_obj)

    def __init__(self, dt_obj):
        self._dt_obj = dt_obj

        self.__day = None
        self.__minute_of_day = None
        self.__century = None

    @property
    def utc_h_offset(self):
        return self._dt_obj.utcoffset().total_seconds() / 3600.0

    @property
    def day(self):
        if self.__day is None:
            y_len = 365.25
            y_offset = 4716
            jm = 30.6001
            d_offset = 1524.5

            year = self._dt_obj.year
            month = self._dt_obj.month
            day = self._dt_obj.day

            if month <= 2:
                year -= 1
                month += 12

            jd_val = int(year / 100)
            jd_val = 2 - jd_val + int(jd_val / 4)

            self.__day = int(y_len * (year + y_offset))
            self.__day += int(jm * (month + 1)) + day + jd_val - d_offset
        return self.__day

    @property
    def minute_of_day(self):
        if self.__minute_of_day is None:
            self.__minute_of_day = self._dt_obj.hour * 60.0
            self.__minute_of_day += self._dt_obj.minute
            self.__minute_of_day += self._dt_obj.second / 60.0
        return self.__minute_of_day

    @property
    def century(self):
        if self.__century is None:
            jd_doc = self.day + self.minute_of_day / 1440.0
            jd_doc -= self.utc_h_offset / 24.0

            y_100_len = 36525.0
            jd_offset = 2451545.0
            self.__century = (jd_doc - jd_offset) / y_100_len
        return self.__century

    def __eq__(self, other):
        if other is None:
            return False
        return self._dt_obj == other._dt_obj

    def __add__(self, other: timedelta):
        if other.total_seconds() == 0:
            return self
        return self.__class__(self._dt_obj + other)

    def __sub__(self, other: timedelta):
        if other.total_seconds() == 0:
            return self
        return self.__class__(self._dt_obj - other)


class SolarSkyPov:
    def __init__(self, pov_latitude, pov_longitude):
        self._longitude = pov_longitude
        self._latitude = pov_latitude

        self._last_datetime = None

        self._tz = pytz.timezone(
            TimezoneFinder().timezone_at(lng=pov_longitude, lat=pov_latitude),
        )

        self._jt = None

        self._apparent_solar_longitude = None
        self._azimuth = None
        self._elevation = None
        self._equation_of_time = None
        self._equation_solar_center = None
        self._ha_rad = None
        self._mean_ecliptic_obliquity = None
        self._mean_solar_anomaly = None
        self._obliquity_correction = None
        self._orbit_eccentricity = None
        self._solar_declination = None
        self._solar_mean_longitude = None
        self._true_solar_longitude = None
        self._zenith = None

    def _calc_mean_ecliptic_obliquity(self):
        seconds = 0.00059 - self._jt.century * 0.001813
        seconds = 46.8150 + self._jt.century * seconds
        seconds = 21.448 - self._jt.century * seconds

        self._mean_ecliptic_obliquity = 23.0 + (26.0 + (seconds / 60.0)) / 60.0

    def _calc_obliquity_correction(self):
        e0 = self._mean_ecliptic_obliquity
        omega = 125.04 - 1934.136 * self._jt.century
        self._obliquity_correction = e0 + 0.00256 * math.cos(
            math.radians(omega),
        )

    def _calc_solar_mean_longitude(self):
        longitude = 280.46646 + self._jt.century * (
            36000.76983 + 0.0003032 * self._jt.century
        )
        while longitude > 360.0:
            longitude -= 360.0
        while longitude < 0.0:
            longitude += 360.0
        self._solar_mean_longitude = longitude

    def _calc_orbit_eccentricity(self):
        o_ecc = 0.000042037 + 0.0000001267 * self._jt.century
        self._orbit_eccentricity = 0.016708634 - self._jt.century * o_ecc

    def _calc_mean_solar_anomaly(self):
        msa = 35999.05029 - 0.0001537 * self._jt.century
        self._mean_solar_anomaly = 357.52911 + self._jt.century * msa

    def _calc_equation_of_time(self):
        epsilon = self._obliquity_correction
        m_long = self._solar_mean_longitude
        e = self._orbit_eccentricity
        m = self._mean_solar_anomaly

        y = math.tan(math.radians(epsilon) / 2.0)
        y *= y

        sin2l0 = math.sin(2.0 * math.radians(m_long))
        sinm = math.sin(math.radians(m))
        cos2l0 = math.cos(2.0 * math.radians(m_long))
        sin4l0 = math.sin(4.0 * math.radians(m_long))
        sin2m = math.sin(2.0 * math.radians(m))

        e_time = y * sin2l0 - 2.0 * e * sinm + 4.0 * e * y * sinm * cos2l0
        e_time -= 0.5 * y**2 * sin4l0 + 1.25 * e**2 * sin2m

        self._equation_of_time = math.degrees(e_time) * 4.0

    def _calc_ha_rad(self):
        zone = self._jt.utc_h_offset

        solar_time_fix = (
            self._equation_of_time + 4.0 * self._longitude - 60.0 * zone
        )
        true_solar_time = self._jt.minute_of_day + solar_time_fix

        while true_solar_time > 1440:
            true_solar_time -= 1440
        hour_angle = true_solar_time / 4.0 - 180.0
        if hour_angle < -180:
            hour_angle += 360.0
        self._ha_rad = math.radians(hour_angle)

    def _calc_equation_solar_center(self):
        mrad = math.radians(self._mean_solar_anomaly)
        sinm = math.sin(mrad)
        sin2m = math.sin(mrad*2)
        sin3m = math.sin(mrad*3)

        esc = 0.004817 + 0.000014 * self._jt.century
        esc = 1.914602 - self._jt.century * esc
        esc *= sinm
        esc += sin2m * (0.019993 - 0.000101 * self._jt.century)
        esc += sin3m * 0.000289

        self._equation_solar_center = esc

    def _calc_true_solar_longitude(self):
        self._true_solar_longitude = (
            self._solar_mean_longitude + self._equation_solar_center
        )

    def _calc_apparent_solar_longitude(self):
        omega = 125.04 - 1934.136 * self._jt.century
        tl = (
                self._true_solar_longitude - 0.00569 - 0.00478 *
                math.sin(math.radians(omega))
        )
        self._apparent_solar_longitude = tl

    def _calc_solar_declination(self):
        e = self._obliquity_correction
        v_lambda = self._apparent_solar_longitude

        sint = math.sin(math.radians(e)) * math.sin(math.radians(v_lambda))
        self._solar_declination = math.degrees(math.asin(sint))

    def _calc_zenith(self):
        csz = (math.sin(math.radians(self._latitude)) *
               math.sin(math.radians(self._solar_declination)) +
               math.cos(math.radians(self._latitude)) *
               math.cos(math.radians(self._solar_declination)) *
               math.cos(self._ha_rad))
        if csz > 1.0:
            csz = 1.0
        elif csz < -1.0:
            csz = -1.0
        self._zenith = math.degrees(math.acos(csz))

    def _calc_elevation(self):
        atmo_elevation = 90.0 - self._zenith
        if atmo_elevation > 85.0:
            ref_correction = 0.0
        else:
            te = math.tan(math.radians(atmo_elevation))
            if atmo_elevation > 5.0:
                ref_correction = 58.1 / te - 0.07 / (te ** 3)
                ref_correction += 0.000086 / (te ** 5)
            elif atmo_elevation > -0.575:
                ref_correction = -12.79 + atmo_elevation * 0.711
                ref_correction = 103.4 + atmo_elevation * ref_correction
                ref_correction = -518.2 + atmo_elevation * ref_correction
                ref_correction = 1735.0 + atmo_elevation * ref_correction

            else:
                ref_correction = -20.774 / te

            ref_correction = ref_correction / 3600.0

        solar_zenith = self._zenith - ref_correction

        self._elevation = (int(100 * (90.0 - solar_zenith))) / 100

    def _calc_azimuth(self):
        az_denom = (math.cos(math.radians(self._latitude)) *
                    math.sin(math.radians(self._zenith)))

        if abs(az_denom) > 0.001:
            az_radius = (
                    ((math.sin(math.radians(self._latitude)) *
                      math.cos(math.radians(self._zenith))) -
                     math.sin(math.radians(self._solar_declination))) /
                    az_denom
            )
            if abs(az_radius) > 1.0:
                if az_radius < 0:
                    az_radius = -1.0
                else:
                    az_radius = 1.0

            azimuth = 180.0 - math.degrees(math.acos(az_radius))

            if self._ha_rad > 0.0:
                azimuth = -azimuth
        else:
            if self._latitude > 0.0:
                azimuth = 180.0
            else:
                azimuth = 0.0
        if azimuth < 0.0:
            azimuth += 360.0

        self._azimuth = (int(100 * azimuth)) / 100

    def set_jt_from_datetime(self, datetime_obj: datetime):
        self.set_julian_time(JulianTime.from_datetime(
            self._tz.localize(datetime_obj),
        ))

    def set_jt_from_strings(self, date: str, time: str):
        self.set_julian_time(JulianTime.from_str(date, time, self._tz))

    def time_travel(self, delta: timedelta):
        self.set_julian_time(self._jt + delta)

    def set_julian_time(self, jt: JulianTime):
        if jt == self._jt:
            return
        self._jt = jt

        self._calc_mean_ecliptic_obliquity()
        self._calc_obliquity_correction()
        self._calc_solar_mean_longitude()
        self._calc_orbit_eccentricity()
        self._calc_mean_solar_anomaly()
        self._calc_equation_of_time()
        self._calc_ha_rad()
        self._calc_equation_solar_center()
        self._calc_true_solar_longitude()
        self._calc_apparent_solar_longitude()
        self._calc_solar_declination()
        self._calc_zenith()
        self._calc_elevation()
        self._calc_azimuth()

    @property
    def time(self):
        return self._jt._dt_obj

    @property
    def azimuth(self):
        return self._azimuth

    @property
    def elevation(self):
        return self._elevation


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-w', '--longitude', required=True, type=float,
                        help='Geographically longitude for the POV.')
    parser.add_argument('-n', '--latitude', required=True, type=float,
                        help='Geographically latitude for the POV.')
    parser.add_argument('-d', '--date', required=False, type=str)
    parser.add_argument('-t', '--local-time', required=False, type=str)
    parser.add_argument('--set-minutes', required=False, type=int)
    parser.add_argument('--set-hours', required=False, type=int)
    parser.add_argument('--set-seconds', required=False, type=int)
    parser.add_argument('--set-total', required=False, type=int, default=1)

    args = parser.parse_args()

    sst = SolarSkyPov(args.latitude, args.longitude)
    delta = timedelta(minutes=args.set_minutes or 0,
                      seconds=args.set_seconds or 0,
                      hours=args.set_hours or 0)

    sst.set_jt_from_strings(args.date, args.local_time)
    for x in range(0, args.set_total):
        print(f'{sst.time}: {sst.azimuth}, {sst.elevation}')
        sst.time_travel(delta)


if __name__ == '__main__':
    main()
