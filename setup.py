import setuptools


description = "Calculates the sun's position for a given place at a given time"
with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="solarposition",
    version="0.0.1",
    author="Tristan Lucas",
    author_email="tristan.lucas.de@gmail.com",
    description=description,
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/TriLucas/solarposition",
    project_urls={
        "Bug Tracker": "https://gitlab.com/TriLucas/solarposition/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "License :: OSI Approved :: GNU General Public License v3",
    ],
    package_dir={"": "src"},
    py_modules=["solarposition"],
    python_requires=">=3.6",
    install_requires=["timezonefinder>=5.2.0", "pytz>=2022.1"],
    entry_points={  # Optional
        "console_scripts": [
            "solarposition=solarposition:main",
        ],
    },
)
